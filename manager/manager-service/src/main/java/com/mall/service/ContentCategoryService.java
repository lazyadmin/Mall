package com.mall.service;

import com.mall.common.pojo.EUTreeNode;
import com.mall.common.pojo.TaotaoResult;

import java.util.List;


public interface ContentCategoryService {

	List<EUTreeNode> getCategoryList(long parentId);
	TaotaoResult insertContentCategory(long parentId, String name);
}
