package com.mall.service;

import com.mall.common.pojo.TaotaoResult;
import com.mall.pojo.TbContent;

public interface ContentService {

	TaotaoResult insertContent(TbContent content);
}
