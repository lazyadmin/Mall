package com.mall.service;

public interface ItemParamItemService {

	String getItemParamByItemId(Long itemId);
}
