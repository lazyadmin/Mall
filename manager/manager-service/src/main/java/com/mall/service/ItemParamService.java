package com.mall.service;

import com.mall.common.pojo.TaotaoResult;
import com.mall.pojo.TbItemParam;


public interface ItemParamService {

	TaotaoResult getItemParamByCid(long cid);
	TaotaoResult insertItemParam(TbItemParam itemParam);
}
