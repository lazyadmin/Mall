package com.mall.service;


import com.mall.common.pojo.EUDataGridResult;
import com.mall.common.pojo.TaotaoResult;
import com.mall.pojo.TbItem;

public interface ItemService {

    TbItem getItemById(long itemId);
    EUDataGridResult getItemList(int page, int rows);
    TaotaoResult createItem(TbItem item, String desc, String itemParam) throws Exception;
}

