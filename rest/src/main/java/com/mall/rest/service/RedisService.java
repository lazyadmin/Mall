package com.mall.rest.service;

import com.mall.common.pojo.TaotaoResult;


public interface RedisService {

	TaotaoResult syncContent(long contentCid);
}
